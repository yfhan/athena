from __future__ import print_function

from AthenaCommon import CfgMgr
from AthenaCommon.CfgGetter import getPrivateTool,getPrivateToolClone,getPublicTool,getPublicToolClone,\
        getService,getServiceClone,getAlgorithm,getAlgorithmClone


def getActsFatrasSimTool(name="ISF_ActsFatrasSimTool", **kwargs):
#    kwargs.setdefault("TrackingGeometryTool" , 'ActsTrackingGeometryTool')
    from ISF_ActsTools.ISF_ActsToolsConf import ISF__ActsFatrasSimTool
    return ISF__ActsFatrasSimTool(name, **kwargs)
