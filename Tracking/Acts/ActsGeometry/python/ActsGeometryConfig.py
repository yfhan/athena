from AthenaCommon.Logging import logging
logging.getLogger().info("Importing %s", __name__)

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from AthenaCommon.GlobalFlags import GlobalFlags
from AthenaCommon import CfgMgr

def ActsTrackingGeometrySvc(name="ActsTrackingGeometrySvc", **kwargs):
     kwargs.setdefault("BuildSubDetectors", {"Pixel", "SCT", "TRT"})
     return CfgMgr.ActsTrackingGeometrySvc(name, **kwargs)

def ActsTrackingGeometryTool(name="ActsTrackingGeometryTool", **kwargs):
     return CfgMgr.ActsTrackingGeometryTool(name, **kwargs)


