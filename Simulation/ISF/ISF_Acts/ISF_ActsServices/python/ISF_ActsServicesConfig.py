# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration


"""
Service and Tool configurations for ISF for ISF_ActsServicesConfig
Yifei Han, 28/02/2020
"""

from __future__ import print_function

from AthenaCommon import CfgMgr
from AthenaCommon.CfgGetter import getPrivateTool,getPrivateToolClone,getPublicTool,getPublicToolClone,\
        getService,getServiceClone,getAlgorithm,getAlgorithmClone

from AthenaCommon.Constants import *  # FATAL,ERROR etc.
from AthenaCommon.SystemOfUnits import *
from AthenaCommon.DetFlags import DetFlags
from TrkDetDescrSvc.TrkDetDescrJobProperties import TrkDetFlags

from ISF_Config.ISF_jobProperties import ISF_Flags # IMPORTANT: Flags must be set before tools are retrieved
from ISF_FatrasServices.ISF_FatrasJobProperties import ISF_FatrasFlags
from ISF_FatrasServices.FatrasTuning import FatrasTuningFlags
from ISF_FatrasServices.FatrasValidation import FatrasValidationFlags
from ISF_Algorithms.collection_merger_helpers import generate_mergeable_collection_name
#do all the same imports as fataras
def getActsSimServiceID(name="ISF_ActsSimSvc", **kwargs):
    kwargs.setdefault("Identifier"      , "Acts")
    kwargs.setdefault("SimulatorTool"  , "ISF_ActsSimulatorToolST")
    from AthenaCommon import CfgMgr
    return CfgMgr.ISF__LegacySimSvc(name, **kwargs )


def getActsSimulatorToolST(name="ISF_ActsSimulatorToolST", **kwargs):
    kwargs.setdefault("IDSimulationTool"  , getPublicTool('ISF_ActsFatrasSimTool'))
    kwargs.setdefault("SimulationTool"  , getPublicTool('ISF_ActsFatrasSimTool'))

    # set the output level
    kwargs.setdefault("OutputLevel"         , ISF_FatrasFlags.OutputLevelGeneral())

    # register Fatras random number stream if not already registered
    from G4AtlasApps.SimFlags import simFlags
    if not simFlags.RandomSeedList.checkForExistingSeed( "FatrasRnd" ):
      simFlags.RandomSeedList.addSeed( "FatrasRnd", 81234740, 23474923 )

    from ISF_ActsServices.ISF_ActsServicesConf import ISF__ActsSimTool
    return ISF__ActsSimTool(name, **kwargs )

