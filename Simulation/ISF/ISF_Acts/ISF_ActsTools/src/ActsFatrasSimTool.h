/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// ActsFatrasSimTool.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef ISF_ACTSTOOLS_ACTSFATRASSIMTOOL_H
#define ISF_ACTSTOOLS_ACTSFATRASSIMTOOL_H

//Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"  // for ToolHandleArray

// ISF
#include "ISF_Interfaces/BaseSimulatorTool.h"
#include "ISF_Interfaces/IParticleFilter.h"
#include "ISF_Interfaces/IParticleProcessor.h"

//ACTS
#include "ActsGeometry/ActsTrackingGeometryTool.h"
#include "ActsGeometryInterfaces/IActsTrackingGeometryTool.h"
#include "ActsGeometry/ActsGeometryContext.h"

#include "Acts/EventData/NeutralParameters.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/StraightLineStepper.hpp"
//#include "Acts/Tests/CommonHelpers/CylindricalTrackingGeometry.hpp"
#include "Acts/Utilities/UnitVectors.hpp"
#include "ActsFatras/Kernel/PhysicsList.hpp"
#include "ActsFatras/Kernel/Simulator.hpp"
#include "ActsFatras/Physics/StandardPhysicsLists.hpp"
#include "ActsFatras/Selectors/ChargeSelectors.hpp"
#include "ActsFatras/Utilities/ParticleData.hpp"

#include "ActsInterop/Logger.h"
//#include "Acts/Utilities/Logger.hpp"

class AtlasDetectorID;
using namespace Acts::UnitLiterals;

namespace ISF { //use fatras namespace for now, to be replaced to ACTS namespace TODO

struct SplitEnergyLoss {
  double splitMomentumMin = 5_GeV;

  template <typename generator_t>
  bool operator()(generator_t&, const Acts::MaterialProperties&,
                  ActsFatras::Particle& particle,
                  std::vector<ActsFatras::Particle>& generated) const {
    const auto p = particle.absMomentum();
    if (splitMomentumMin < p) {
      particle.setAbsMomentum(0.5 * p);
      const auto pid = particle.particleId().makeDescendant();
      generated.push_back(particle.withParticleId(pid).setAbsMomentum(0.5 * p));
    }
    // never break
    return false;
  }
};

 class IHitCreator;
      
  /**
     @class ACTSFATRASSIMTOOL
     
     Standard ATLAS hit creator, with ACTS-fatras simulation
     
     @author yi.fei.han at cern.ch
  */
  
  class ActsFatrasSimTool: public extends<AthAlgTool, ISF::IParticleProcessor>
  {
  public:
    // setup propagator-related types
    // use the default navigation
    using Navigator = Acts::Navigator;
    using MagneticField = Acts::ConstantBField;
    // propagate charged particles numerically in a constant magnetic field
    using ChargedStepper = Acts::EigenStepper<MagneticField>;
    using ChargedPropagator = Acts::Propagator<ChargedStepper, Navigator>;
    // propagate neutral particles with just straight lines
    using NeutralStepper = Acts::StraightLineStepper;
    using NeutralPropagator = Acts::Propagator<NeutralStepper, Navigator>;
    
    // setup simulator-related types
    // the random number generator type
    using Generator = std::ranlux48;
    // all charged particles w/ a mock-up physics list and hits everywhere
    using ChargedSelector = ActsFatras::ChargedSelector;
    using ChargedPhysicsList =
        ActsFatras::PhysicsList<ActsFatras::detail::StandardScattering,
                                SplitEnergyLoss>;

    // single particle simulator
    using ChargedSimulator =
        ActsFatras::ParticleSimulator<ChargedPropagator, ChargedPhysicsList,
                                      ActsFatras::EverySurface>;
    // all neutral particles w/o physics and no hits
    using NeutralSelector = ActsFatras::NeutralSelector;
    using NeutralSimulator =
        ActsFatras::ParticleSimulator<NeutralPropagator, ActsFatras::PhysicsList<>,
                                      ActsFatras::NoSurface>;
    // full simulator type for charged and neutrals
    using Simulator = ActsFatras::Simulator<ChargedSelector, ChargedSimulator,
                                            NeutralSelector, NeutralSimulator>;

    // single particle simulator
     

    /**Constructor */
    ActsFatrasSimTool(const std::string&,const std::string&,const IInterface*);
    
    /**Destructor*/
    ~ActsFatrasSimTool();
    
    /** AlgTool initailize method.*/
    StatusCode initialize();
    
    /** AlgTool finalize method */
    StatusCode finalize();
    

    virtual ISF::ISFParticle* process( const ISFParticle& isp, CLHEP::HepRandomEngine* ) const override;

    virtual StatusCode setupEvent()  { return StatusCode::SUCCESS; }; //override

    virtual StatusCode releaseEvent()  { return StatusCode::SUCCESS; }; //override

    virtual ISF::SimulationFlavor simFlavor() const  { return ISF::Acts; }; //override
    
  private:   
    /** Used to find out the sub-det from */
    std::string                             m_idHelperName;             
    const AtlasDetectorID*                  m_idHelper;
    ToolHandle<IActsTrackingGeometryTool>   m_actsTrackingGeometryTool{this, "TrackingGeometryTool", "ActsTrackingGeometryTool"};

    ActsGeometryContext m_geoCtx;
    Acts::MagneticFieldContext m_magCtx;

    Acts::Logging::Level m_logLevel = Acts::Logging::Level::INFO;

    // construct the example detector
    std::shared_ptr<const Acts::TrackingGeometry> m_trackingGeometry ;

    // construct the propagators
    Navigator m_navigator{std::move(m_trackingGeometry)};
    ChargedStepper m_chargedStepper{Acts::ConstantBField(0, 0, 1_T)};
    ChargedPropagator m_chargedPropagator{std::move(m_chargedStepper), m_navigator};
    NeutralPropagator m_neutralPropagator{NeutralStepper(), m_navigator};

    // construct the simulator
    ChargedSimulator m_chargedSimulator;
//    NeutralSimulator m_neutralSimulator;
//    Simulator m_simulator;
 
    // Sim Hit Creators
//    ToolHandle<iFatras::ISimHitCreator>          m_simHitCreatorID;
//    ToolHandle<iFatras::ISimHitCreator>          m_simHitCreatorMS;
  };
} // end of ISF namespace

#endif 
