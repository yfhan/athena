/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
#include <algorithm>
#include <random>

#include "ActsFatrasSimTool.h"
#include "ActsGeometry/ActsTrackingGeometryTool.h"
#include "ActsGeometryInterfaces/IActsTrackingGeometryTool.h"
#include "ActsInterop/Logger.h"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"
#include "CLHEP/Random/RandFlat.h"

using namespace Acts::UnitLiterals;

ISF::ActsFatrasSimTool::ActsFatrasSimTool(const std::string& type, const std::string& name,  const IInterface* parent)
  : base_class(type, name, parent),
    m_chargedSimulator(std::move(m_chargedPropagator), (makeActsAthenaLogger(this, "CylVolBldr", "ActsTGSvc")))
//    m_neutralSimulator(std::move(m_neutralPropagator), (makeActsAthenaLogger(this, "CylVolBldr", "ActsTGSvc")))
 //   m_simulator(std::move(m_chargedSimulator), std::move(m_neutralSimulator))  

  //    m_simHitCreatorID(""),
//    m_simHitCreatorMS("")
{
  declareProperty("ActsTrackingGeometryTool", m_actsTrackingGeometryTool);
  //hit creation tools
//  declareProperty( "SimHitCreatorID", m_simHitCreatorID); 
//  declareProperty( "SimHitCreatorMS", m_simHitCreatorMS);
}

ISF::ActsFatrasSimTool::~ActsFatrasSimTool() {

}

StatusCode ISF::ActsFatrasSimTool::initialize() {

//  ATH_CHECK( BaseSimulatorTool::initialize() );
  
  if (!m_actsTrackingGeometryTool.empty() && m_actsTrackingGeometryTool.retrieve().isFailure()) {
    ATH_MSG_FATAL( "[ --- ] Could not retrieve " << m_actsTrackingGeometryTool );
    return StatusCode::FAILURE;
  }
  
  m_trackingGeometry = m_actsTrackingGeometryTool->trackingGeometry();
  ATH_MSG_INFO("YIFEI1 this m_actsTrackingGeometryTool->trackingGeometry() is " << m_actsTrackingGeometryTool->trackingGeometry());
  ATH_MSG_INFO("YIFEI1 this m_trackingGeomtry is " << m_trackingGeometry);

  m_geoCtx = m_actsTrackingGeometryTool->getNominalGeometryContext();
  m_navigator = Navigator(std::move(m_trackingGeometry));

  m_chargedPropagator = ChargedPropagator (std::move(m_chargedStepper), m_navigator);
  m_chargedSimulator = ChargedSimulator (std::move(m_chargedPropagator), makeActsAthenaLogger(this, "CylVolBldr", "ActsTGSvc"));
//  m_neutralSimulator = NeutralSimulator(std::move(m_neutralPropagator),  makeActsAthenaLogger(this, "CylVolBldr", "ActsTGSvc"));
//  m_simulator = Simulator (std::move(m_chargedSimulator), std::move(m_neutralSimulator));
 

  ATH_MSG_INFO("YIFEI this msgSvc() is " <<this->msgSvc());
 //retreive hit creators
//  if (retrieveTool<iFatras::ISimHitCreator>(m_simHitCreatorID).isFailure())
//      return StatusCode::FAILURE;
//  if (retrieveTool<iFatras::ISimHitCreator>(m_simHitCreatorMS).isFailure())
//      return StatusCode::FAILURE;
  

 
  return StatusCode::SUCCESS;
}

StatusCode ISF::ActsFatrasSimTool::finalize() {

  //ATH_CHECK( BaseSimulatorTool::finalize() );
 
 return StatusCode::SUCCESS;
}


ISF::ISFParticle* ISF::ActsFatrasSimTool::process( const ISFParticle& isp, CLHEP::HepRandomEngine* ) const {

  // give a screen output that you entered ActsFatrasSimSvc
  ATH_MSG_VERBOSE( "Particle " << isp << " received for simulation." );

  // prepare simulation call parameters
  // random number generator
  Generator generator;
  // input/ output particle and hits containers
  std::vector<ActsFatras::Particle> input;
  std::vector<ActsFatras::Particle> simulatedInitial;
  std::vector<ActsFatras::Particle> simulatedFinal;
  std::vector<ActsFatras::Hit> hits;

    // create input particles. particle number should ne non-zero.
    // just create 1 particle to test
  
  const auto pid = ActsFatras::Barcode().setVertexPrimary(42).setParticle(isp.barcode());  
  auto particle =
      ActsFatras::Particle(pid, (Acts::PdgParticle)isp.pdgCode(), isp.charge(), isp.mass()*10e-3 )
          .setDirection(Acts::makeDirectionUnitFromPhiEta(isp.momentum().phi(), isp.momentum().eta()))
          .setAbsMomentum(isp.momentum().mag()*10e-3)
          .setPosition4(isp.position().x(), isp.position().y(), isp.position().z(), isp.timeStamp());
//          .setPosition4(0, 0, 0, 0);
  input.push_back(std::move(particle));

  /** do acts simulation **/
// simulate multiple particles
//  auto result = m_simulator.simulate(m_geoCtx.any(), m_magCtx, generator, input,
//                                   simulatedInitial, simulatedFinal, hits);
// simulate single particle
  auto result  = m_chargedSimulator.simulate(m_geoCtx.any(), m_magCtx, generator, particle);
//  auto result = result_and_msg.first;
//  auto msg = result_and_msg.second;
  if(particle.charge()!=0){
    ATH_MSG_INFO("YIFEI ActsFatrasSimTool called ");
    ATH_MSG_INFO("YIFEI result" << result << "end") ; 
//    ATH_MSG_INFO("YIFEI TrackingGeometry is "<<m_navigator.trackingGeometry); 
  }
//  if(!result.ok()){ ATH_MSG_INFO("YIFEI Error code  " << result.error()); 
//                    ATH_MSG_INFO("YIFEI-------------------------------");}

//  if(result.ok() && particle.mass() > 0 && result.value().particle.mass() > 0)  {
/*
  if(result.ok())  {
    ATH_MSG_INFO("YIFEI particle charge  " << particle.charge());
    ATH_MSG_INFO("YIFEI particle mass is " << particle.mass());
    ATH_MSG_INFO("YIFEI particle pt   is " << particle.transverseMomentum());
    ATH_MSG_INFO("YIFEI particle postion " << particle.position()[0] << " " << particle.position()[1] << " " << particle.position()[2]);
//    ATH_MSG_INFO("YIFEI result is Alive  " << result.isAlive);
//    ATH_MSG_INFO("YIFEI result pathinX0 " << result.pathInX0);
    ATH_MSG_INFO("YIFEI -------------------------------");
  }
*/
//  ATH_MSG_INFO("YIFEI particle initial size" <<simulatedInitial.size());
//  if(simulatedInitial.size()>0)  ATH_MSG_INFO("YIFEI particle initial m" <<simulatedInitial.at(0).mass());

//  ATH_MSG_INFO("YIFEI particle final size" <<simulatedFinal.size());
//  if(simulatedFinal.size()>0)  ATH_MSG_INFO("YIFEI particle final m" <<simulatedFinal.at(0).mass());


  /** record hits here some how **/
  int hitCounter = 0;
//  ATH_MSG_INFO("YIFEI Hits size is " << hits.size());
  for (auto hit : hits){
//      hitCounter ++;
//TODO      //convert to athena hits and record hits
//      ATH_MSG_INFO("YIFEI Hit counter is " << hitCounter << " and Hit time " << hit.time() << " and hit X " << hit.position()[1]);
//      if(hitCounter > 10) break;
  }
//  if (result) {
    // new particle into the stack
    //secondaries.push_back(newIsp); //don't push back acts particle for now
//  }

  // ActsFatras call done
  return 0; //return nothing for now
}
